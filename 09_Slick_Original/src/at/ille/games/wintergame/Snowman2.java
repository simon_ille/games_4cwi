package at.ille.games.wintergame;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;

public class Snowman2 {

	private float x, y, speed;

	private Image visibleImage, frontImage, rightImage, leftImage;
	
	private boolean rightKey, leftKey;

	public Snowman2() throws SlickException {

		this.frontImage = new Image("C:\\Users\\simon\\Pictures\\Camera Roll\\SchneemanVorne.png");		
		this.rightImage = new Image("C:\\Users\\simon\\Pictures\\Camera Roll\\SchneemannRechts.png");
		this.leftImage = new Image("C:\\Users\\simon\\Pictures\\Camera Roll\\SchneemannLinks.png");

		x = 600;
		y = 225;
		speed = 0.3f;

	}

	public void update(GameContainer gc, int delta) {
		Input input = gc.getInput();
		
		this.rightKey = gc.getInput().isKeyDown(Input.KEY_RIGHT);
		this.leftKey = gc.getInput().isKeyDown(Input.KEY_LEFT);
		
		if (input.isKeyDown(Input.KEY_UP)) {
			y -= delta * this.speed;
		}

		if (input.isKeyDown(Input.KEY_DOWN)) {
			y += delta * this.speed;
		}

		if (input.isKeyDown(Input.KEY_LEFT)) {
			x -= delta * this.speed;
		}

		if (input.isKeyDown(Input.KEY_RIGHT)) {
			x += delta * this.speed;
		}
	}

	public void render(Graphics graphics) {
		if (rightKey == true) {
			graphics.drawImage(rightImage, x, y); // fillOval(100,100,100,100);
		}
		else if (leftKey == true) {
			graphics.drawImage(leftImage, x, y);
		} else {
			graphics.drawImage(frontImage, x, y);
		}

	}

}
