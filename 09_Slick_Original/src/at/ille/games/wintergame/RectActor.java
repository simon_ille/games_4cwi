package at.ille.games.wintergame;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class RectActor {
  private double x, y;
  private boolean rectUp = false;
  private boolean rectDown = false;
  private boolean rectLeft = false;
  private boolean rectRight = true;

  public RectActor(double x, double y) {
    super();
    this.x = x;
    this.y = y;
  }

  public void update(GameContainer gc, int delta) {
    if (rectRight) {
      this.x++;
      if (x >= 700) {
        this.rectRight = false;
        this.rectDown = true;
      }
    }

    if (rectDown) {
      this.y++;
      if (y >= 500) {
        this.rectDown = false;
        this.rectLeft = true;
      }
    }

    if (rectLeft) {
      this.x--;
      if (x <= 50) {
        this.rectLeft = false;
        this.rectUp = true;
      }
    }

    if (rectUp) {
      this.y--;
      if (y <= 50) {
        this.rectUp = false;
        this.rectRight = true;
      }
    }
  }

  public void render(Graphics graphics) {
    graphics.fillRect((float) this.x, (float) this.y, 50, 50);
  }

}