package at.ille.games.wintergame;

import java.util.Random;

import org.newdawn.slick.Color;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.geom.Polygon;
import org.newdawn.slick.geom.Shape;
import org.newdawn.slick.geom.Transform;

public class ShootingStar {

	private float tolerance, x, y;
	private float fx, fy, speed;
	private Shape source;
	private Random r;
	private int timeCounter;
	private int randomCount;

	public ShootingStar() {

		// this.x = 300;
		// this.y = 100;

		this.fx = 0;
		this.fy = 0;
		this.speed = 0.4f;
		this.timeCounter = 0;
		this.r = new Random();

		Polygon source = new Polygon();
		source.addPoint(100, 100);
		source.addPoint(105, 115);
		source.addPoint(120, 115);
		source.addPoint(109, 125);
		source.addPoint(115, 140);
		source.addPoint(100, 130);
		source.addPoint(85, 140);
		source.addPoint(91, 125);
		source.addPoint(60, 120);
		source.addPoint(20, 130);
		source.addPoint(30, 120);
		source.addPoint(20, 113);
		source.addPoint(65, 108);
		source.addPoint(95, 115);

		this.source = (Shape) source;

		this.randomCount = r.nextInt(6000) + 3000;

		System.out.println(this.toString());
	}

	public void update(GameContainer gc, int delta) {
		this.fx += delta * speed;
		float ly = 0.001f * this.fx * this.fx;

//		tolerance = (float) Math.sin(y * 0.2f) * 3;
//		//System.out.println(tolerance);
//
//		x += tolerance;
		this.source.setCenterX(this.x + this.fx);
		// this.source.setCenterY(100);
		this.source.setCenterY(this.y + ly);
		this.source = this.source.transform(Transform.createRotateTransform(0.008f, this.x, 100));

		checkIfShootingStarMustBeSpawned(delta);
		//System.out.println(timeCounter);
	}

	private void checkIfShootingStarMustBeSpawned(int delta) {
		if (this.timeCounter <= this.randomCount) {
			this.timeCounter += delta;
		} else {
			this.x = r.nextInt(300);
			this.y = -100;
			this.fx = 0;
			this.fy = 0;
			this.timeCounter = 0;
		}
	}

	public void init() {

	}

	public void render(Graphics graphics) {
		graphics.setColor(Color.yellow);
		graphics.fill(this.source);// (this.x + tolerance, this.y, 30, 30);
		graphics.setColor(Color.white);
	}

	@Override
	public String toString() {
		return "ShootingStar [tolerance=" + tolerance + ", x=" + x + ", y=" + y + ", fx=" + fx + ", fy=" + fy
				+ ", speed=" + speed + ", source=" + source + ", r=" + r + ", timeCounter=" + timeCounter
				+ ", randomCount=" + randomCount + ", randomSpawnX=]";
	}

}
