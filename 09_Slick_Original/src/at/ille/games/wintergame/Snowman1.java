package at.ille.games.wintergame;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;

public class Snowman1 {

	private float x, y, speed, ballX, ballY;

	private Image visibleImage, frontImage, rightImage, leftImage;

	private boolean rightKey, leftKey, keySpace, isVisible, go;

	public Snowman1() throws SlickException {

		this.frontImage = new Image("C:\\Users\\simon\\Pictures\\Camera Roll\\SchneemanVorne.png");
		this.rightImage = new Image("C:\\Users\\simon\\Pictures\\Camera Roll\\SchneemannRechts.png");
		this.leftImage = new Image("C:\\Users\\simon\\Pictures\\Camera Roll\\SchneemannLinks.png");

		x = 0;
		y = 225;
		speed = 0.3f;

		this.isVisible = false;
		this.go = false;

	}

	public void update(GameContainer gc, int delta) {
		Input input = gc.getInput();

		this.rightKey = gc.getInput().isKeyDown(Input.KEY_D);
		this.leftKey = gc.getInput().isKeyDown(Input.KEY_A);

		if (input.isKeyDown(Input.KEY_W)) {
			y -= delta * this.speed;
		}

		if (input.isKeyDown(Input.KEY_S)) {
			y += delta * this.speed;
		}

		if (input.isKeyDown(Input.KEY_A)) {
			x -= delta * this.speed;
		}

		if (input.isKeyDown(Input.KEY_D)) {
			x += delta * this.speed;
		}

		if (gc.getInput().isKeyDown(Input.KEY_SPACE)) {
			this.ballX = 0;
			isVisible = true;
			go = true;
		}
		
		if (go == true) {
			if (gc.getInput().isKeyDown(Input.KEY_D)) {
				this.ballX += delta;
			} else if (gc.getInput().isKeyDown(Input.KEY_A)) {
				this.ballX -= delta;
			} else {
				this.ballX += delta;
			}
		}
		
		if (this.ballX > 800 || this.ballX < -800) {
			go = false;
			isVisible = false;
		}

	}

	public void render(Graphics graphics) {
		if (rightKey == true) {
			graphics.drawImage(rightImage, x, y); // fillOval(100,100,100,100);
		} else if (leftKey == true) {
			graphics.drawImage(leftImage, x, y);
		} else {
			graphics.drawImage(frontImage, x, y);
		}

		if (isVisible == true) {
			graphics.fillOval(this.ballX + x + 30, y + 30, 20, 20);
		}

	}

}
