package at.ille.games.wintergame;

import java.awt.Polygon;
import java.util.ArrayList;
import java.util.List;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

public class MainGame extends BasicGame {

	private Image backgroundImage;
	private CircleActor ca1;
	private RectActor ra1;
	private OvalActor oa1;
	private ShootingStar ss;
	private Snowman1 sm1;
	private Snowman2 sm2;
	private List<Snowflake> snowflakes;
	
	public MainGame(String title) {
		super(title);
		// TODO Auto-generated constructor stub

	}

	@Override
	public void render(GameContainer gc, Graphics graphics) throws SlickException {
		// gezeichnet
		backgroundImage.draw(0,0);
		//this.ca1.render(graphics);
	    //this.ra1.render(graphics);
	    //this.oa1.render(graphics);
		this.ss.render(graphics);
	    for (Snowflake s : this.snowflakes) {
			s.render(graphics);
		}
	    this.sm1.render(graphics);
	    this.sm2.render(graphics);
	}

	@Override
	public void init(GameContainer arg0) throws SlickException {
		backgroundImage = new Image("C:\\Users\\simon\\Pictures\\Camera Roll\\HintergrundSchnee.png");
		
		this.ca1 = new CircleActor(375, 0);
	    this.ra1 = new RectActor(50, 50);
	    this.oa1 = new OvalActor(50, 400);
	    this.ss = new ShootingStar();
	    this.sm1 = new Snowman1();
	    this.sm2 = new Snowman2();

	    this.snowflakes = new ArrayList<>();
	    
	    for (int i=0; i<=500; i++) {
	    	this.snowflakes.add(new Snowflake(0));
	    }
	    
	    for (int i=0; i<=250; i++) {
	    	this.snowflakes.add(new Snowflake(1));
	    }

	    for (int i=0; i<=200; i++) {
	    	this.snowflakes.add(new Snowflake(2));
	    }
	    
	}
	    
	@Override
	public void update(GameContainer gc, int delta) throws SlickException {
		// delta ist die Zeit seit dem letzten Aufruf
	    this.ca1.update(gc, delta);
	    this.ra1.update(gc, delta);
	    this.oa1.update(gc, delta);
	    this.ss.update(gc, delta);
	    this.sm1.update(gc, delta);
	    this.sm2.update(gc, delta);
	    for(Snowflake s : this.snowflakes) {
	    	s.update(gc, delta);
	    }
	    
	}

	public static void main(String[] argv) {
		try {
			AppGameContainer container = new AppGameContainer(new MainGame("Simons Game :)"));
			container.setDisplayMode(800, 600, false);
			container.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}

}
