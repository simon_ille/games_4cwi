package at.ille.games.wintergame;

import java.util.Random;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class Snowflake {

	private float x, y, size, speed, randomspeed, bool, tolerance;
	private Random r;

	// 0 = klein
	// 1 = mittel
	// 2 = gro�

	public Snowflake(int size) {

		this.r = new Random();

		this.x = r.nextInt(800);
		this.y = r.nextInt(600) * -1;

		if (size == 0) {
			this.size = 20;
			this.randomspeed = r.nextFloat();
			this.speed = r.nextFloat() - 0.88f;
		}

		if (size == 1) {
			this.size = 30;
			this.speed = r.nextFloat() - 0.84f;
		}

		if (size == 2) {
			this.size = 40;
			this.speed = r.nextFloat() - 0.8f;
		}

	}

	public void update(GameContainer gc, int delta) {
		this.y = this.y + (delta * this.speed);
		if (this.y > 600) {
			this.x = r.nextInt(800);
			this.y = r.nextInt(600) * -1;
		}
		
		tolerance = (float) Math.sin(y*0.02f) * 60;
		//System.out.println(tolerance);
		//this.x = this.x + ((delta * 0.2f) * (this.yspeed + (r.nextFloat() * 10)/**for(int i=0; i<100; i++) {r.nextFloat();}**/ - (r.nextFloat() * 10)));
	}

	public void render(Graphics graphics) {
		graphics.fillOval(this.x + tolerance, this.y, this.size, this.size);

	}

}
