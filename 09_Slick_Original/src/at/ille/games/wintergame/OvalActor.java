package at.ille.games.wintergame;

import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;

public class OvalActor {
  private double x, y;
  private boolean ovalRight = true;
  private boolean ovalLeft = false;

  public OvalActor(double x, double y) {
    super();
    this.x = x;
    this.y = y;
  }

  public void update(GameContainer gc, int delta) {
    if (ovalRight) {
      this.x++;
      if (x >= 650) {
        this.ovalRight = false;
        this.ovalLeft = true;
      }
    }

    if (ovalLeft) {
      this.x--;
      if (x <= 50) {
        this.ovalLeft = false;
        this.ovalRight = true;
      }
    }
  }

  public void render(Graphics graphics) {
    graphics.fillOval((float) this.x, (float) this.y, 100, 50);
  }

}