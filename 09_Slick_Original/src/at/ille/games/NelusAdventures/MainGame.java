package at.ille.games.NelusAdventures;

import org.newdawn.slick.AppGameContainer;
import org.newdawn.slick.BasicGame;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

import at.ille.games.wintergame.MainGame;



public class MainGame extends BasicGame {

	public Image backgroundImage;
	
	public MainGame() {
		super("NelusAdventures");
	}
	
	@Override
	public void render(GameContainer arg0, Graphics arg1) throws SlickException {
		backgroundImage.draw(0,0);
		
	}

	@Override
	public void init(GameContainer arg0) throws SlickException {
		backgroundImage = new Image("C:\\Users\\simon\\Pictures\\Camera Roll\\SnowLandscape.png");
		
	}

	@Override
	public void update(GameContainer arg0, int arg1) throws SlickException {
		// TODO Auto-generated method stub
		
	}
	
	public static void main(String[] argv) {
		try {
			AppGameContainer container = new AppGameContainer(new MainGame());
			container.setDisplayMode(1920, 1080, false);
			container.start();
		} catch (SlickException e) {
			e.printStackTrace();
		}
	}

}
